#include <stdio.h>

char caracteres[27]={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z'};


int main()
{
    printf("Alfabeto español en ASCII.");
    for(int i=0;i<27;i++){
        printf("\n %c",caracteres[i]);
        printf(": %d",caracteres[i]);
    }
    return 0;
}
